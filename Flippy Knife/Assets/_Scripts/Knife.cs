﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Knife : MonoBehaviour
{

    private Rigidbody rb;
    private Vector2 startSwipe;
    private Vector2 endSwipe;
    private Camera myCamera;
    private float startSwipeTime;
    private int score = 0;
    private Text scoretxt;
    private Text highScore;
    private int HighScore;
    private bool inAir = false;

    public float force = 5f;
    public float torque = -20f;

	// Use this for initialization
	void Start () {
        myCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        rb = GetComponent<Rigidbody>();
        scoretxt = GameObject.Find("Score").GetComponent<Text>();
        highScore = GameObject.Find("High Score").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        scoretxt.text = (score - 1).ToString();
        HighScore = PlayerPrefs.GetInt("highScore" , 0);
        HighScore -= 1;
        highScore.text = HighScore.ToString();

        if (score > PlayerPrefs.GetInt("highScore" , 0))
        {
            PlayerPrefs.SetInt("highScore", score);

        }

        if (inAir)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            startSwipe = myCamera.ScreenToViewportPoint(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            endSwipe = myCamera.ScreenToViewportPoint(Input.mousePosition);
            Swipe();
        }



	}

    void Swipe()
    {
        startSwipeTime = Time.time;
        Vector2 swipe = endSwipe - startSwipe;
        rb.isKinematic = false;

        rb.AddForce(swipe * force, ForceMode.Impulse);
        rb.AddTorque(0f , 0f , torque , ForceMode.Impulse);
        inAir = true;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Block")
        {
            inAir = false;
            rb.isKinematic = true;
            score += 1;   
        }
        else
        {
            Restart();
        }
    }

    void OnCollisionEnter(Collision col)
    {
        float timeInAir = Time.time - startSwipeTime;

        if (!rb.isKinematic && timeInAir >= 0.05f)
        {
            Restart();
        }
    }

    void Restart()
    {
        inAir = false;
        score = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
